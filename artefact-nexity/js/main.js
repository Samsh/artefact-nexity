'use strict';

/*
var API_ENDPOINT = "https://www.nexity.fr/ws-rest/contact.json",
    ContactLabels = {
        CALL_ME: "callme",
        RDV: "rdv",
        ASK_INFO: "askinfo"
    },
    MailSubjects = {};

function clickPopin(t) {
    $("#contact_popin").removeClass("success");
    var e = $(t.currentTarget).attr("id");
    $("#contact_popin").find(".modal-title").text($(t.currentTarget).text()), $("#contactform").find('[name="objet_mail"]').val(MailSubjects[e])
}

function isCPValid(t) {
    return /((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))[0-9]{3}/.test(t)
}

function isTelephoneValid(t) {
    return /(((0|(00|\+)33[-. ]?)[1-9]([-. ]?[0-9]{2}){4})|((00|\+)(?!33|0)([-. ]?[0-9])+)+)/.test(t)
}

function clickSubmit(t) {
    t.preventDefault();
    var e = $("#contactform");
    e.find(".form-group,.accept").removeClass("has-error");
    for (var a = ["nom", "prenom", "telephone", "codepostal", "email"], o = !0, i = {}, n = e.serializeArray(), s = 0; s < n.length; ++s) i[n[s].name] = n[s].value;
    for (var c = 0; c < a.length; ++c) i[a[c]] || (e.find('[name="' + a[c] + '"]').parents(".form-group").addClass("has-error"), o = !1);
    if (o) {
        var r = e.find('[name="codepostal"]');
        isCPValid(r.val()) || (o = !1, r.parents(".form-group").addClass("has-error"))
    }
    if (o) {
        var l = e.find('[name="telephone"]');
        isTelephoneValid(l.val()) || (o = !1, l.parents(".form-group").addClass("has-error"))
    }
    i.hasOwnProperty("optin_nexity") || (e.find(".accept-nexity").addClass("has-error"), o = !1), i.hasOwnProperty("optin_partenaire") || (e.find(".accept-partenaire").addClass("has-error"), o = !1), o && $.post(API_ENDPOINT, i, function(t) {
        console.log(t.status), "ok" === t.status ? (e.get(0).reset(), $("#contact_popin").addClass("success")) : alert(t.message)
    })
}

function initContact() {
    for (var t, e = document.location.search.split("&"), a = 0; a < e.length; ++a) "ctcsrc" === (t = e[a].split("="))[0].toLowerCase() && $("#contactform").find('[name="ctcsrc"]').val(t[1]);
    for (var o in ContactLabels) $("#" + ContactLabels[o]).click(clickPopin);
    $("#contactform").find('[type="submit"]').click(clickSubmit)
}
MailSubjects[ContactLabels.CALL_ME] = "Call me", MailSubjects[ContactLabels.RDV] = "Prendre RDV", MailSubjects[ContactLabels.ASK_INFO] = "Ask info", 
*/

/* *************************************** */
//Change la couleur de la barre du menu au scroll
$(window).scroll(function() {
  var scroll = $(window).scrollTop(); // how many pixels you've scrolled
  var os = $('#firstHeader').offset().top; // pixels to the top of div1
  var ht = $('#firstHeader').height(); // height of div1 in pixels
  // if you've scrolled further than the top of div1 plus it's height
  // change the color. either by adding a class or setting a css property
  if (scroll > os + ht - 70) {
    $('.navbar').addClass('blueMenu');
  } else {
    $('.navbar').removeClass('blueMenu');
  }
});

// Detect ios 11_x_x affected
// NEED TO BE UPDATED if new versions are affected
var ua = navigator.userAgent,
  iOS = /iPad|iPhone|iPod/.test(ua),
  iOS11 = /OS 11_0_1|OS 11_0_2|OS 11_0_3|OS 11_1|OS 11_1_1|OS 11_1_2|OS 11_2|OS 11_2_1/.test(
    ua
  );

// ios 11 bug caret position
if (iOS && iOS11) {
  // Add CSS class to body
  $('body').addClass('iosBugFixCaret');
}

/*
			// Slider Media
			$('#contentSlider').slick({
				dots: true,
				customPaging: function(contentSlider, i) { 
					return '<button class="tab">' + $(contentSlider.$slides[i]).attr('title') + '</button>';
				},
				slidesToShow: 1,
				slidesToScroll: 1,
				autoplay: false,					
				infinite: true,
				speed: 300,
				centerMode: true,
				arrows: false,
				variableWidth: false,
				adaptiveHeight: true,
				fade: true,
				cssEase: 'linear'
				
			});*/
// align dots to left margin
var $margin = $('.limitWidth');
var marge = $margin.css('margin-left');
$('.slick-dots').css('margin-left', marge);

/*
			$( ".slick-slide" ).change(function() {
			  alert( "Handler for .change() called." );
			});
			*/

/*
			$('#contentSlider').on('afterChange', function(event, slick, currentSlide){
			 
				if(currentSlide == 1){
					$('#slide_2_right_content #smartphone_plombier').addClass('msgAnim')
				}else{
					$('#slide_2_right_content #smartphone_plombier').removeClass('msgAnim')
				}
			  
				if(currentSlide == 2){			
					$('#slide_3_center_content #conterner_plombier #notification').addClass('msgNotifMsg');
					$('#nb_notification_contener').addClass('msgNotifBloc');
					setTimeout(function(){ 
						$('#notificatoinChange').text(Number($('#notificatoinChange').text())+1);
					}, 3000);
					setTimeout(function(){ 
						$('#notificatoinChange').text(Number($('#notificatoinChange').text())+1);
					}, 4000);
				}else{
					$('#slide_3_center_content #conterner_plombier #notification').removeClass('msgNotifMsg');
					$('#nb_notification_contener').removeClass('msgNotifBloc');
					$('#notificatoinChange').text('1');
				}
			}); */

var heightScroll = -90;

if ($(window).width() <= 768) {
  heightScroll = -90;
}

$('.logement').click(function() {
  $('html, body').animate(
    {
      scrollTop: $('#slide_1').offset().top + heightScroll
    },
    1500
  );
  //$('#slick-slide-control00').click();

  $('#notificatoinChange').text('1');
  if ($(window).width() < 1024) {
    $('.navbar-toggle').click();
  }
});
$('.residence').click(function() {
  $('html, body').animate(
    {
      scrollTop: $('#slide_2').offset().top + heightScroll
    },
    1500
  );
  //$('#slick-slide-control01').click();

  if ($(window).width() < 1024) {
    $('.navbar-toggle').click();
  }
});
$('.quartier').click(function() {
  $('html, body').animate(
    {
      scrollTop: $('#slide_3').offset().top + heightScroll
    },
    1500
  );
  //$('#slick-slide-control02').click();
  if ($(window).width() < 1024) {
    $('.navbar-toggle').click();
  }
});
$('.contacter').click(function() {
  $('html, body').animate(
    {
      scrollTop: $('#contact_footer').offset().top
    },
    1500
  );

  if ($(window).width() < 1024) {
    $('.navbar-toggle').click();
  }

  setTimeout(function() {
    $('#slide_2_right_content #smartphone_plombier').removeClass('msgAnim');
    $('#slide_3_center_content #conterner_plombier #notification').removeClass(
      'msgNotifMsg'
    );
    $('#nb_notification_contener').removeClass('msgNotifBloc');
  }, 2000);
});

if ($(window).width() <= 414) {
  var leftSlide = 27;
} else {
  var leftSlide = 37;
}

$('#lumContener').click(function() {
  $('#slideLum').addClass('lumOn');
  $('#slideLum').animate(
    {
      left: leftSlide
    },
    500
  );
  var videoFile = 'video/lumieres.mp4';
  $('#lecteurSmartphone source').attr('src', videoFile);
  $('#lecteurSmartphone')[0].load();
  $('#lecteurSmartphone')
    .get(0)
    .play();
  setTimeout(function() {
    $('#slideLum').removeClass('lumOn');
    $('#slideLum').animate(
      {
        left: 3
      },
      500
    );
  }, 8000);
});

$('#volContener').click(function() {
  $('#slideVol').addClass('volOn');
  $('#slideVol').animate(
    {
      left: leftSlide
    },
    500
  );
  var videoFile = 'video/volet.mp4';
  $('#lecteurSmartphone source').attr('src', videoFile);
  $('#lecteurSmartphone')[0].load();
  $('#lecteurSmartphone')
    .get(0)
    .play();
  setTimeout(function() {
    $('#slideVol').removeClass('volOn');
    $('#slideVol').animate(
      {
        left: 3
      },
      500
    );
  }, 8000);
});
/*
	$("#chaleur").click(function() {	
		var n = 25; // Nombre final du compteur
		var cpt = 20; // Initialisation du compteur
		var duree = 20; // Durée en seconde pendant laquel le compteur ira de 0 à 15
		var delta = Math.ceil((duree * 1000) / n); // On calcule l'intervalle de temps entre chaque rafraîchissement du compteur (durée mise en milliseconde)
		var node =  document.getElementById("chaleur"); // On récupère notre noeud où sera rafraîchi la valeur du compteur
		 
		function countdown() {
		  node.innerHTML = ++cpt;
		  if( cpt < n ) { // Si on est pas arrivé à la valeur finale, on relance notre compteur une nouvelle fois
			 setTimeout(countdown, delta);
		  }
		}		 
		setTimeout(countdown, delta);
		
		$("#chaleur").addClass('volOn');
		var videoFile = 'video/chauffage.mp4';
		$('#lecteurSmartphone source').attr('src', videoFile);
		$("#lecteurSmartphone")[0].load()
		$('#lecteurSmartphone').get(0).play()
		
	});*/

$('#moins').click(function() {
  $('#chaleur').text(Number($('#chaleur').text()) - 1);
});
$('#plus').click(function() {
  $('#chaleur').text(Number($('#chaleur').text()) + 1);
});

function isInView(elem) {
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();

  var elemTop = $(elem).offset().top;
  var elemBottom = elemTop + $(elem).height();

  return elemBottom <= docViewBottom && elemTop >= docViewTop;
}

$(window).scroll(function() {
  if (isInView($('.smart1'))) {
    $('#slide_2_right_content #smartphone_plombier').addClass('msgAnim');
  }
});

var nomNotif = 0;
$(window).scroll(function() {
  if (isInView($('.smart2'))) {
    $('#slide_3_center_content #conterner_plombier #notification').addClass(
      'msgNotifMsg'
    );
    $('#nb_notification_contener').addClass('msgNotifBloc');

    if (nomNotif < 1) {
      setTimeout(function() {
        $('#notificatoinChange').text(
          Number($('#notificatoinChange').text()) + 1
        );
      }, 3000);
      setTimeout(function() {
        $('#notificatoinChange').text(
          Number($('#notificatoinChange').text()) + 1
        );
      }, 4000);
      nomNotif = 1;
    }
  }
});
